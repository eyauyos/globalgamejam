﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NetworkUIController : MonoBehaviour
{
    [Header("Lobby")]
    public GameObject loginPanel;
    public InputField nickName;
    public Button loginBtn;

    [Header("Create and Join")]
    public GameObject createAndJoinPanel;
    public InputField createRoomInputField;
    public Button createRoomBtn;
    public InputField JoinRoomInputField;
    public Button JoinRoomBtn;

    [Header("Loading")]
    public GameObject loadingPanel;

    void Start()
    {
        loginBtn.onClick.AddListener(Login);
        createRoomBtn.onClick.AddListener(CreateRoom);
        JoinRoomBtn.onClick.AddListener(JoinRoom);
        PUNConnectionManager.OnPhotonStateChange += PUNStateChange;
    }


    private void Login()
    {

        loadingPanel.SetActive(true);
        AppManager.instance.currentUserName = nickName.text.Trim();
        PUNConnectionManager.Instance.ConnectToMaster(nickName.text.Trim());
    }

    private void CreateRoom()
    {
        //PUNConnectionManager.Instance.CreateRoom(createRoomInputField.text);
        PUNConnectionManager.Instance.CreateRoom(AppManager.instance.m_RoomName,Convert.ToByte(AppManager.instance.m_MaxNumberOfPLayers));
    }

    private void JoinRoom()
    {
        //PUNConnectionManager.Instance.ConnectToRoom(JoinRoomInputField.text);
        Debug.Log("Join");
        PUNConnectionManager.Instance.ConnectToRoom(AppManager.instance.m_RoomName);
    }

    private void PUNStateChange(PUNConnectionManager.PhotonState state)
    {
        switch (state)
        {
            case PUNConnectionManager.PhotonState.Disconnected:
                break;
            case PUNConnectionManager.PhotonState.OnConnectedToMaster:
                break;
            case PUNConnectionManager.PhotonState.OnJoinedLobby:
                loginPanel.SetActive(false);
                CreateRoom();
                //createAndJoinPanel.SetActive(true);
                break;
            case PUNConnectionManager.PhotonState.OnJoinedRoom:
                loadingPanel.SetActive(false);
                break;
            case PUNConnectionManager.PhotonState.OnJoinRoomFailed:
                break;
            case PUNConnectionManager.PhotonState.OnLeftRoom:
                break;
            case PUNConnectionManager.PhotonState.OnLeftLobby:
                break;
            case PUNConnectionManager.PhotonState.OnCreateRoom:
                break;
            case PUNConnectionManager.PhotonState.OnCreateRoomFailed:
                break;
            case PUNConnectionManager.PhotonState.OnPlayersInRoomUpdated:
                break;
            default:
                break;
        }
    }

}
