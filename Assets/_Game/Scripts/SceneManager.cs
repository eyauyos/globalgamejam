﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace demo.Controllers
{
    public class SceneManager : MonoBehaviour
    {

        public static event LoadProgress OnLoadProgress;
        public delegate void LoadProgress(float progress);
        public static event Loaded OnSceneLoaded;
        public delegate void Loaded(Scene scene, LoadSceneMode loadmode);

        public static SceneManager instance;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
                DontDestroyOnLoad(this.gameObject);
            }
            else if (instance != this)
                Destroy(this.gameObject);

            UnityEngine.SceneManagement.SceneManager.sceneLoaded += SceneLoaded;            
        }

        /// <summary>
        /// Gets the currently active Scene
        /// </summary>
        /// <returns></returns>
        public Scene GetActiveScene() {
            return UnityEngine.SceneManagement.SceneManager.GetActiveScene();
        }

        public void SetActiveScene(string targetScene) 
        {
            Scene target = UnityEngine.SceneManagement.SceneManager.GetSceneByName(targetScene);
            UnityEngine.SceneManagement.SceneManager.SetActiveScene(target);
        }

        /// <summary>
        /// Handle event when scene is loaded
        /// </summary>
        /// <param name="arg0"></param>
        /// <param name="arg1"></param>
        private void SceneLoaded(Scene arg0, LoadSceneMode arg1)
        {
            OnSceneLoaded?.Invoke(arg0, arg1);
        }       

        /// <summary>
        /// Load scene normally by name
        /// </summary>
        /// <param name="sceneName"></param>
        public void LoadScene(string sceneName)
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(sceneName,LoadSceneMode.Single);
        }

        /// <summary>
        /// Load Scene asynchronously and return operation to hanlde allowSceneActivation
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="OnSceneReady"></param>
        public void LoadSceneAsync(string sceneName, Action<float> Progress, Action<AsyncOperation> OnSceneReady) 
        {
            var asyncO=UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName);
            asyncO.allowSceneActivation = false;

            StartCoroutine(HandleProgress(asyncO, Progress));

            asyncO.completed += (ao) =>
            {
                OnSceneReady?.Invoke(ao);
            };
        }

        /// <summary>
        /// Load Scene aditively asynchronously and return operation to hanlde allowSceneActivation
        /// </summary>
        /// <param name="sceneName"></param>
        /// <param name="OnSceneReady"></param>
        public void LoadSceneAsyncAditive(string sceneName, Action<float> Progress, Action<AsyncOperation> OnSceneReady) 
        {
            var asyncO = UnityEngine.SceneManagement.SceneManager.LoadSceneAsync(sceneName,LoadSceneMode.Additive);            
            StartCoroutine(HandleProgress(asyncO, Progress));

            asyncO.completed += (ao) =>
            {
                OnSceneReady?.Invoke(ao);
            };
        }

        /// <summary>
        /// Coroutine that handle loading progress
        /// </summary>
        /// <param name="asyncO"></param>
        /// <returns></returns>
        private IEnumerator HandleProgress( AsyncOperation asyncO, Action<float> OnProgress=null)
        {
            while (!asyncO.isDone) 
            {
                OnProgress?.Invoke(asyncO.progress / 0.9f);
                OnLoadProgress?.Invoke(asyncO.progress / 0.9f);
                yield return null;
            }
            yield return null;
        }
    }
}