﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioControllerPlayer : MonoBehaviour
{
    public static AudioControllerPlayer Instance;
    public AudioClip m_Walk;
    public AudioClip m_SwingPunch;
    public AudioClip m_HitPunch;
    public AudioClip m_StunSFX;
    public AudioClip m_KeySFX;
    public AudioClip m_PowerUpSFX;
    public AudioSource m_AudioSourceSFX;
    public AudioSource m_AudioSourceWalkSFX;

    private void Awake()
    {
        Instance = this;
    }
    public void Walk(bool play)
    {
        if (play)
        {
            m_AudioSourceWalkSFX.clip = m_Walk;
            m_AudioSourceWalkSFX.pitch = Random.Range(1,1.5f);
            //m_AudioSourceWalkSFX.loop = true;
            m_AudioSourceWalkSFX.Play();
        }
        else
        {
            m_AudioSourceWalkSFX.Stop();
        }
        
    }

    public void SwingPunch()
    {
        m_AudioSourceSFX.clip = m_SwingPunch;
        m_AudioSourceSFX.pitch = Random.Range(1, 1.5f);
        m_AudioSourceSFX.Play();
    }

    public void HitPunch()
    {
        m_AudioSourceSFX.clip = m_HitPunch;
        m_AudioSourceSFX.pitch = Random.Range(1, 1.5f);
        m_AudioSourceSFX.Play();
    }

    public void KeySFX()
    {
        m_AudioSourceSFX.clip = m_KeySFX;
        m_AudioSourceSFX.pitch = Random.Range(1, 1.5f);
        m_AudioSourceSFX.Play();
    }

    public void StunSFX()
    {
        m_AudioSourceWalkSFX.clip = m_StunSFX;
        m_AudioSourceWalkSFX.pitch = Random.Range(1, 1.5f);
        m_AudioSourceWalkSFX.Play();
    }

    public void PowerUpSFX()
    {
        m_AudioSourceSFX.clip = m_PowerUpSFX;
        m_AudioSourceSFX.pitch = Random.Range(1, 1.5f);
        m_AudioSourceSFX.Play();
    }
}
