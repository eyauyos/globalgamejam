﻿using Photon.Pun;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AppManager : MonoBehaviour
{
    public static AppManager instance;

    public int m_MaxNumberOfPLayers = 3;
    public string m_RoomName;

    [Header("scenes")]
    public string loginScene;
    public string GameScene;

    public string currentUserName;
    public PlayerController player;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)
            Destroy(this.gameObject);

        demo.Controllers.SceneManager.OnSceneLoaded += OnSceneLoaded;
        PUNConnectionManager.OnPhotonStateChange += PUNStateChange;
    }

   

    private void Start()
    {
        demo.Controllers.SceneManager.instance.LoadScene(loginScene);
    }

    private void PUNStateChange(PUNConnectionManager.PhotonState state)
    {
        switch (state)
        {
            case PUNConnectionManager.PhotonState.Disconnected:
                break;
            case PUNConnectionManager.PhotonState.OnConnectedToMaster:
                break;
            case PUNConnectionManager.PhotonState.OnJoinedLobby:
                break;
            case PUNConnectionManager.PhotonState.OnJoinedRoom:
                PhotonNetwork.LoadLevel(GameScene);
                Debug.Log("<color=yellow> se ha unido a la sala </color>");
                break;
            case PUNConnectionManager.PhotonState.OnJoinRoomFailed:
                break;
            case PUNConnectionManager.PhotonState.OnLeftRoom:
                break;
            case PUNConnectionManager.PhotonState.OnLeftLobby:
                break;
            case PUNConnectionManager.PhotonState.OnCreateRoom:
                break;
            case PUNConnectionManager.PhotonState.OnCreateRoomFailed:
                break;
            case PUNConnectionManager.PhotonState.OnPlayersInRoomUpdated:
                break;
            default:
                break;
        }

    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode loadmode)
    {
        if(scene.name == GameScene)
        {
            PUNConnectionManager.Instance.InstantiatePlayer();
            player = PUNConnectionManager.Instance.GetPlayer().GetComponent<PlayerController>();
            player.SetData(currentUserName);
            player.gameObject.transform.position = Vector3.zero;
        }
    }
}
