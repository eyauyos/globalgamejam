﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoweUpController : MonoBehaviour
{
    public static PoweUpController Instance;

    public List<GameObject> powerUpList = new List<GameObject>();

    private void Start()
    {
        Instance = this;
    }

    public void EnableAndDisablePowerUp(string powerUpName, bool value)
    {
        for (int i = 0; i < powerUpList.Count; i++)
        {
            if (powerUpList[i].name == powerUpName)
                powerUpList[i].SetActive(value);
        }
    }

}
