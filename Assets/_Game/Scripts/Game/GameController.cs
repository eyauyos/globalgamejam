﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public static GameController Instance;
    //Game state
    public static bool m_IsGameActive = false;
    public static bool m_IsGameReadyToStart = false;
    public static string m_GameWinnerName = "No Winner";
    public GameObject m_VictoryPanel;
    public Text m_WinnerText;

    //Timer
    public static float m_CurrentTime;
    public static float m_StartTime = 100.0f;
    private float m_LimitTime = 20.0f;
    public Text m_Timer;
    public List<Transform> m_PlayerPositions;

    //Sounds
    private int m_CurrentSoundIndex = 0;

    //attributes
    public GameObject m_AttibutesPanel;
    public Image m_HitIcon;

    public AudioSource m_AudioSource;
    public AudioClip[] m_AudioClips;

    //Final Animation
    public GameObject m_FinalAnimation;
    public Text m_WinnerName;
    public SkinnedMeshRenderer m_WinnerColorMesh;
    public static Color m_WinnerColor;
    public Camera m_Camera;

    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        m_WinnerText.text = "Waiting players...";
        m_AudioSource = GetComponent<AudioSource>();
        m_AudioSource.clip = m_AudioClips[m_CurrentSoundIndex];
        m_AudioSource.Play();
    }

    // Update is called once per frame
    void Update()
    {
        if(Photon.Pun.PhotonNetwork.CurrentRoom.PlayerCount == AppManager.instance.m_MaxNumberOfPLayers && !m_IsGameReadyToStart)
        {
            m_IsGameReadyToStart = true;
            StartCoroutine(InitializeCronometer());
        }

        if (m_IsGameActive)
        {
            UpdateCronometer();
        }

        if(Input.GetKeyDown(KeyCode.T))
        {
            StartGame();
        }
    }

    IEnumerator InitializeCronometer()
    {
        m_VictoryPanel.SetActive(true);
        m_WinnerText.text = "Ready!!";
        yield return new WaitForSeconds(1.0f);
        m_WinnerText.text = "3";
        yield return new WaitForSeconds(1.0f);
        m_WinnerText.text = "2";
        yield return new WaitForSeconds(1.0f);
        m_WinnerText.text = "1";
        yield return new WaitForSeconds(1.0f);
        m_WinnerText.text = "Go!!";
        yield return new WaitForSeconds(1.0f);
        m_VictoryPanel.SetActive(false);
        m_AttibutesPanel.SetActive(true);
        StartGame();
    }

    public void StartGame()
    {
        m_CurrentTime = m_StartTime;
        m_IsGameActive = true;

        KeysRespawnController.Instance.SpawnKeys();
    }

    public void UpdateCronometer()
    {
        m_CurrentTime -= Time.deltaTime;
        float minutes = Mathf.Floor(m_CurrentTime / 60.0f);
        float seconds = Mathf.RoundToInt(m_CurrentTime % 60.0f);

        if (seconds == 60)
        {
            minutes++;
        }

        string minutesString = minutes < 10 ? "0" + minutes.ToString() : minutes.ToString();
        string secondsString = seconds < 10 ? "0" + seconds.ToString() : seconds.ToString();
        secondsString = secondsString == "60" ? "00" : secondsString;
        m_Timer.text = "00:" + minutesString + ":" + secondsString;


        if (m_CurrentTime <= m_LimitTime && m_CurrentSoundIndex == 0)
        {
            ChangeMusic();
        }

        if (m_CurrentTime <= 0.0f)
        {
            m_Timer.text = "00:00:00";
            FinishGame();
        }
    }

    public void ChangeMusic()
    {
        Debug.Log("SpeedMusic");
        m_CurrentSoundIndex++;
        m_AudioSource.clip = m_AudioClips[m_CurrentSoundIndex];
        m_AudioSource.Play();
    }

    public void FinishGame()
    {
        m_IsGameActive = false;       
        //m_VictoryPanel.SetActive(true);
        m_AttibutesPanel.SetActive(false);

        if(m_GameWinnerName != "No Winner")
        {
            m_WinnerText.text = "The winner is " + m_GameWinnerName;

            m_Camera.gameObject.SetActive(false);
            m_WinnerName.text = m_GameWinnerName;
            m_WinnerColorMesh.materials[0].color = m_WinnerColor;
            m_FinalAnimation.SetActive(true);

            m_AudioSource.clip = m_AudioClips[0];
            m_AudioSource.Play();

            StartCoroutine(FinalGame());
        }
        else
        {
            m_AudioSource.clip = m_AudioClips[2];
            m_AudioSource.Play();
            m_WinnerText.text = m_GameWinnerName;
            m_VictoryPanel.SetActive(true);
            Invoke("RestartGame", 5.0f);
        }
    }

    IEnumerator FinalGame()
    {
        yield return new WaitForSeconds(10.0f);
        m_VictoryPanel.SetActive(true);
        yield return new WaitForSeconds(5.0f);
        RestartGame();
    }

    public void RestartGame()
    {
        //Restart
        m_IsGameReadyToStart = false;
        ResetPlayerPosition();
        m_FinalAnimation.SetActive(false);
        m_Camera.gameObject.SetActive(true);
        m_AudioSource.clip = m_AudioClips[0];
        m_AudioSource.Play();
        StartCoroutine(InitializeCronometer());
    }

    public void ResetPlayerPosition()
    {
        PlayerController[] players = FindObjectsOfType<PlayerController>();
        for(int i = 0; i < players.Length; i++)
        {
            players[i].SetInitPlayerPosition();
        }
    }

    public void EnableAndDisableHit(int value)
    {
        if (value >= 1)
            m_HitIcon.color = Color.red;
        else
            m_HitIcon.color = Color.white;
    }
}
