﻿using Photon.Pun;
using Photon.Realtime;
using System;
using System.Collections.Generic;
using UnityEngine;

public class PUNConnectionManager : MonoBehaviourPunCallbacks
{
    #region EVENTS

    public static event PlayerConnectedToRoom OnPlayerConnectedToRoom;
    public delegate void PlayerConnectedToRoom(Player player);

    public static event PlayerLeftRoom OnPlayerDisconnectedFromRoom;
    public delegate void PlayerLeftRoom(Player player);

    public static event RetriveRoomList OnRetrieveRoomList;
    public delegate void RetriveRoomList(List<RoomInfo> roomlist);

    public static event MasterClientChanged OnMasterClientChanged;
    public delegate void MasterClientChanged(Player newMasterClient);

    public static event PhotonStateChange OnPhotonStateChange;
    public delegate void PhotonStateChange(PhotonState state);

    #endregion

    #region PUBLIC VARIABLES
    public static PUNConnectionManager Instance;
    public static string CurrentRoomName;
    public static string CurrentRoomPassword;
    public static string CurrentPlayerName;
    public static int PlayersCount;
    public static ExitGames.Client.Photon.Hashtable RoomProperties;
    public static PhotonState State = PhotonState.Disconnected;
    #endregion

    #region PRIVATE VARIABLES
    public List<RoomInfo> rooms;
    private PhotonView player;
    #endregion

    #region STATES
    public enum PhotonState
    {
        Disconnected,
        OnConnectedToMaster,
        OnJoinedLobby,
        OnJoinedRoom,
        OnJoinRoomFailed,
        OnLeftRoom,
        OnLeftLobby,
        OnCreateRoom,
        OnCreateRoomFailed,
        OnPlayersInRoomUpdated
    }
    #endregion

    #region MONOBEHAVIOUR METHODS
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (Instance != this)
            Destroy(this.gameObject);
    }
    #endregion

    #region PUBLIC METHODS
    public void ConnectToMaster(string NickName)
    {
        rooms = new List<RoomInfo>();
        PhotonNetwork.LocalPlayer.NickName = NickName != "" ? NickName : "Player" + UnityEngine.Random.Range(0, 100);
        PhotonNetwork.GameVersion = "1";
        PhotonNetwork.ConnectUsingSettings();
    }

    public void InstantiatePlayer()
    {
        GameObject go = PhotonNetwork.Instantiate("Player", Vector3.zero, Quaternion.identity);
        player = go.GetComponent<PhotonView>();
    }

    public void ClearMyPlayerProperties() {
        PhotonNetwork.LocalPlayer.CustomProperties.Clear();
    }

    public void DisconnectPhoton()
    {
        PhotonNetwork.Disconnect();
    }

    public void SetMyPlayerProperty(string key, string keyValue)
    {
        ExitGames.Client.Photon.Hashtable hashtable = PhotonNetwork.LocalPlayer.CustomProperties;
        if (hashtable.ContainsKey(key))
        {
            hashtable[key] = keyValue;
        }
        else
        {
            hashtable.Add(key, keyValue);
        }
        PhotonNetwork.LocalPlayer.SetCustomProperties(hashtable);
    }

    public T GetMyPlayerProperty<T>(string key) 
    {
        ExitGames.Client.Photon.Hashtable hashtable = PhotonNetwork.LocalPlayer.CustomProperties;
        if (hashtable.ContainsKey(key))
        {
            return (T)Convert.ChangeType(hashtable[key], typeof(T));
        }
        else
        {
            return default;
        }
    }

    /// <summary>
    /// Sets room properties. IMPORTANT: This only works if called BEFORE using CreateRoom.
    /// </summary>
    /// <param name="key">Name of the key</param>
    /// <param name="keyValue">Value of the key</param>
    public void PrepareRoomProperties(string key, object keyValue)
    {
        if (RoomProperties == null)
        {
            RoomProperties = new ExitGames.Client.Photon.Hashtable();
        }

        if (RoomProperties.ContainsKey(key))
        {
            RoomProperties[key] = keyValue;
        }
        else
        {
            RoomProperties.Add(key, keyValue);
        }
    }

    public void ClearRoomProperties()
    {
        if(RoomProperties!=null)
            RoomProperties.Clear();
    }

    public void CreateRoom(string roomName, byte maxPlayer)
    {
        RoomOptions ro = new RoomOptions();
        ro.IsOpen = true;
        ro.IsVisible = true;
        ro.MaxPlayers = maxPlayer;
        //ro.PlayerTtl = 10000;

        ro.CustomRoomProperties = RoomProperties;

        PhotonNetwork.JoinOrCreateRoom(roomName, ro, TypedLobby.Default);
    }

    public void ConnectToRoom(string roomName)
    {
        PhotonNetwork.JoinRoom(roomName);
    }

    public void LeaveRoom()
    {
        PhotonNetwork.LeaveRoom();

        
    }

    public PhotonView GetPlayer()
    {
        return player;
    }
    #endregion

    #region PUNCALLBACKS
    public override void OnConnectedToMaster()
    {
        Debug.Log("OnConnectedToMaster");
        UpdateConnectionState(PhotonState.OnConnectedToMaster);
        PhotonNetwork.JoinLobby();
    }

    public override void OnDisconnected(DisconnectCause cause)
    {
        Debug.Log("OnDisconected");
        UpdateConnectionState(PhotonState.Disconnected);
    }

    public override void OnJoinedLobby()
    {
        Debug.Log("OnJoinedLobby");
        UpdateConnectionState(PhotonState.OnJoinedLobby);
    }

    public override void OnJoinedRoom()
    {
        CurrentRoomName = PhotonNetwork.CurrentRoom.Name;
        CurrentPlayerName = PhotonNetwork.LocalPlayer.NickName;
        Debug.Log("OnJoinedRoom");
        UpdateConnectionState(PhotonState.OnJoinedRoom);
        Debug.Log("Photon Network Room Name " + PhotonNetwork.CurrentRoom.Name.Split('_')[0]);
        Debug.Log("Photon Network Players " + PhotonNetwork.CurrentRoom.PlayerCount);
    }

    public override void OnRoomListUpdate(List<RoomInfo> roomList)
    {
        Debug.Log("On list Room updated " + roomList.Count);
        Debug.Log("On list Room updated " + roomList.Count);

        foreach (var item in roomList)
        {
            if (!item.RemovedFromList)
            {              
                if (!rooms.Contains(item))
                {
                    rooms.Add(item);
                }
                else
                {
                    if (DifferentPlayers(item))
                    {
                        rooms.Remove(item);
                        rooms.Add(item);
                    }
                }
            }
            else
            {              
                rooms.Remove(item);
            }
        }
       
       
        OnRetrieveRoomList?.Invoke(rooms);
    }

    public override void OnPlayerEnteredRoom(Player newPlayer)
    {
        PlayersCount = PhotonNetwork.CurrentRoom.PlayerCount;
        UpdateConnectionState(PhotonState.OnPlayersInRoomUpdated);
        Debug.Log("Player entered room" + newPlayer.NickName);
        OnPlayerConnectedToRoom?.Invoke(newPlayer);
    }

    public override void OnPlayerLeftRoom(Player otherPlayer)
    {
        PlayersCount = PhotonNetwork.CurrentRoom.PlayerCount;
        UpdateConnectionState(PhotonState.OnPlayersInRoomUpdated);
        Debug.Log("Player left room " + otherPlayer.NickName);
        OnPlayerDisconnectedFromRoom?.Invoke(otherPlayer);
    }

    public override void OnJoinRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnJoinRoomFailed " + message);
        UpdateConnectionState(PhotonState.OnJoinRoomFailed);
    }

    public override void OnCreateRoomFailed(short returnCode, string message)
    {
        Debug.Log("OnCreateRoomFailed " + message);
        UpdateConnectionState(PhotonState.OnCreateRoomFailed);
    }

    public override void OnCreatedRoom()
    {
        Debug.Log("OnCreateRoom");
        UpdateConnectionState(PhotonState.OnCreateRoom);
        Debug.Log("Room created as " + PhotonNetwork.CurrentRoom.Name.Split('_')[0]);
    }

    public override void OnLeftRoom()
    {
        Debug.Log("OnLeftRoom");
        UpdateConnectionState(PhotonState.OnLeftRoom);
    }

    public override void OnMasterClientSwitched(Player newMasterClient)
    {
        Debug.Log("Host migration - new master client:" + newMasterClient);
        OnMasterClientChanged(newMasterClient);
    }
    #endregion

    #region PRIVATE METHODS
    private void UpdateConnectionState(PhotonState state)
    {
        State = state;
        OnPhotonStateChange?.Invoke(state);
    }
   
    private bool DifferentPlayers(RoomInfo roominfo) 
    {
        bool dif = false;
        foreach (var item in rooms)
        {
            if (item.PlayerCount != roominfo.PlayerCount)
                dif = true;
        }
        return dif;
    }
    #endregion
}
