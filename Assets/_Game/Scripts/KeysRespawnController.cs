﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeysRespawnController : MonoBehaviour
{
    public static KeysRespawnController Instance;

    public int m_KeysCount;
    public List<GameObject> keys;
    public List<Transform> KeySpawn;


    private void Awake()
    {
        Instance = this;
        
    }
    private void Start()
    {
        foreach (var key in keys)
        {
            key.SetActive(false);
        }

        
            
    }

    public void SpawnKeys()
    {
        foreach (var key in keys)
        {
            key.SetActive(true);
        }
    }

    public void RandomSetKeysPositions(int indexKey,string nameParent)
    {
        foreach (var item in KeySpawn)
        {
            if(item.name == nameParent)
            {
                keys[indexKey].transform.parent = item;
                keys[indexKey].transform.localPosition = Vector3.zero;
                keys[indexKey].SetActive(true);
                keys[indexKey].GetComponent<KeyController>().ChangeState(KeyController.keyState.free,0);

                if (item.GetComponent<BoxCollider>() == null)
                {
                    item.gameObject.AddComponent<BoxCollider>();
                    item.gameObject.GetComponent<BoxCollider>().isTrigger = true;
                    gameObject.transform.GetChild(0).gameObject.SetActive(true);
                }

            }
        }
    }

    public void SetKeysPositions(int indexKey, string nameParent)
    {
        foreach (var item in KeySpawn)
        {

        }
    }
    public void GG(int photonId, int i, string nameParent)
    {
        if (RelocateKeys(photonId))
        {
            RandomSetKeysPositions(i, nameParent);
        }
    }

    //nuevo método
    public void RestoreKey(int photonId, string nameKey)
    {
        if (RelocateKeys(photonId))
        {
            EnabledKey(nameKey);
        }
    }

    //NO USADO
    public bool RelocateKeys(int photonId)
    {
        foreach (var key in keys)
        {
            var id = key.GetComponent<KeyController>().whoHasIt;
            if (photonId == id)
                return true;
        }
        return false;
    }

    //NO USADO
    public void DestroyParentIdentifier(string nameParent)
    {
        foreach (var item in KeySpawn)
        {
            if (item.name == nameParent)
            {
                Destroy(item.gameObject.GetComponent<BoxCollider>());
                gameObject.transform.GetChild(0).gameObject.SetActive(false);
            }
        }
    }

    public void UnabledKey(string nameKey,int idPlayer)
    {
        foreach (var key in keys)
        {
            if (key.name == nameKey)
            {
                key.SetActive(false);
                key.GetComponent<KeyController>().ChangeState(KeyController.keyState.occupied, idPlayer);

            }
        }
    }

    public void EnabledKey(string nameKey)
    {
        foreach (var key in keys)
        {
            if (key.name == nameKey)
            {
                key.SetActive(true);
                key.GetComponent<KeyController>().ChangeState(KeyController.keyState.free, 0);
            }
        }
    }

    /*
    public Transform AvailableRandomPosition(List<Transform> parent)
    {
        int randomIndex = Random.Range(0, parent.Count - 1);
        if (parent[randomIndex].gameObject.GetComponent<BoxCollider>() == null)
        {
            parent[randomIndex].gameObject.AddComponent<BoxCollider>();
            parent[randomIndex].gameObject.GetComponent<BoxCollider>().isTrigger = true;
            return parent[randomIndex];
        }
        else
            return AvailableRandomPosition(parent);
    }
    */
    public Transform GetKeySpawnAvailable()
    {
        List<Transform> available = new List<Transform>();
        foreach (var item in KeySpawn)
        {
            if (item.gameObject.GetComponent<BoxCollider>() == null)
                available.Add(item);
        }

        int randomIndex = Random.Range(0, available.Count - 1);
        available[randomIndex].gameObject.AddComponent<BoxCollider>();
        available[randomIndex].gameObject.GetComponent<BoxCollider>().isTrigger = true;
        gameObject.transform.GetChild(0).gameObject.SetActive(true);
        return available[randomIndex];
    }

}
