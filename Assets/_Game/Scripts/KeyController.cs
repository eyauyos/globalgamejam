﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyController : MonoBehaviour
{
    public enum keyState
    {
        free,
        occupied
    }
    public int whoHasIt;

    public keyState state;

    public Transform keyPositionRef;

    private void Start()
    {
        gameObject.transform.position = keyPositionRef.position;
    }

    public keyState getKeyState()
    {
        return state;
    }

    public void ChangeState( keyState key, int name)
    {
        state = key;
        whoHasIt = name;
    }


}
