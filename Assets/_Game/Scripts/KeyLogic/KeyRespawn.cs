﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyRespawn : MonoBehaviour
{
    public KeyRespawn Instance;
    public int m_KeysCount;

    [SerializeField]
    private Transform SpawnKeysContainer;
    [SerializeField]
    private GameObject keyPrefab;
    private List<GameObject> keys;
    private List<Transform> KeySpawn;


    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        keys = new List<GameObject>();
        KeySpawn = new List<Transform>();
        for (int i = 0; i < SpawnKeysContainer.childCount; i++)
        {
            KeySpawn.Add(SpawnKeysContainer.GetChild(i));
        }

        for (int i = 0; i < m_KeysCount; i++)
        {
            GameObject temp_key = Instantiate(keyPrefab);
            temp_key.SetActive(false);
            keys.Add(temp_key);
        }
        
    }

    public void RandomSetKeysPositions()
    {
       
        foreach (var key in keys)
        {
            //int randomIndex = Random.Range(0, index.Count - 1);//equisde
            key.transform.parent = AvailableRandomPosition(KeySpawn.ToArray());
            key.transform.localPosition = Vector3.zero;
            key.SetActive(true);
        }


    }
    public void RandomSetKeyReposition(string id)
    {
        foreach (var key in keys)
        {
            if (key.GetComponent<KeyController>().state == KeyController.keyState.occupied)
            {
                key.transform.parent = AvailableRandomPosition(KeySpawn.ToArray());
                key.transform.localPosition = Vector3.zero;
                key.SetActive(true);
            }
            
        }
    }

    private Transform AvailableRandomPosition(Transform[] parent)
    {
        int randomIndex = Random.Range(0, parent.Length - 1);
        if (parent[randomIndex].childCount == 0)
            return parent[randomIndex];
        else
            return AvailableRandomPosition(parent);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            RandomSetKeysPositions();
        }
    }
}
