﻿using Photon.Pun;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviourPun
{
    public Text nameText;
    public Camera cam;

    [Header("Movement")]
    [SerializeField]
    private CharacterController characterController;
    public float speed = 5;
    public float turnSmoothVelocity = 0.12f;
    public float turnSmoothTime = 0.12f;
    public Animator anim;

    private float currentSpeed;
    private float speedSmoothTime = 0.1f;
    private float speedSmoothVelocity = 0.12f;
    private bool isStun = false;


    [Header("Rotation")]
    public Transform lookAtCamera;
    public float mouseSensitivity = 5;
    public float distanceFromTarget = 5;
    public Vector2 pitchMinMax = new Vector2(9,70);

    public float rotationSmoothTime = 0.12f;

    private Vector3 rotationSmoothVelocity;
    private Vector3 currentRotation;

    private float yaw = 0;
    private float pitch = 0;

    [Header("Attack")]
    public bool hasPoweUp;
    public string currentNamePoweUp;
    public GameObject objectAttack;

    [Header("Keys")]
    public List<GameObject> keysInBag;
    public int numberOfKeys;
    public bool completeKey;

    private List<string> nameKeysInBag;
    public Color[] m_PlayerColors;
    public SkinnedMeshRenderer m_PlayerMesh;

    private Vector3 positionInitPlayer;

    private void Start()
    {
        positionInitPlayer = transform.position;
        cam = Camera.main;


        nameKeysInBag = new List<string>();
        int numberPlayers = PhotonNetwork.CurrentRoom.PlayerCount;
        gameObject.transform.position= GameController.Instance.m_PlayerPositions[numberPlayers-1].position;

        if(photonView.IsMine)
        {
            photonView.RPC("RPC_SetColorToNameTag", RpcTarget.AllBuffered, numberPlayers - 1);
        }
        
        //MANEJAREMOS AHORA POSICIONES ESTÁTICAS

        //if (PhotonNetwork.IsMasterClient)
        //{
        //    if (photonView.IsMine)
        //    {
        //        for (int i = 0; i < KeysRespawnController.Instance.keys.Count; i++)
        //        {
        //            var pos = KeysRespawnController.Instance.GetKeySpawnAvailable().gameObject.name;

        //            photonView.RPC("RPC_KeysRespawn", RpcTarget.AllBuffered, i, pos);
        //        }
        //    }
        //}
    }


    private void Update()
    {
        MovementPlayer();
        RotatePlayer();
        AttackPlayer();

        nameText.gameObject.transform.parent.transform.LookAt(cam.transform);
    }


    private void RotatePlayer()
    {
        if (photonView.IsMine && !isStun)
        {
            if (Input.GetMouseButton(0))
            {
                yaw += Input.GetAxis("Mouse X") * mouseSensitivity;
                pitch -= Input.GetAxis("Mouse Y") * mouseSensitivity;
                pitch = Mathf.Clamp(pitch, pitchMinMax.x, pitchMinMax.y);

            }
                currentRotation = Vector3.SmoothDamp(currentRotation, new Vector3(pitch, yaw), ref rotationSmoothVelocity, rotationSmoothTime);

                //Camera.main.transform.eulerAngles = currentRotation;
                cam.transform.position = lookAtCamera.position - cam.transform.forward * distanceFromTarget;
           
        }
    }

    private void MovementPlayer()
    {
        if (photonView.IsMine && !isStun && GameController.m_IsGameActive)
        {
            float x = Input.GetAxis("Horizontal");
            float z = Input.GetAxis("Vertical");

            Vector2 inputDir = new Vector2(x, z).normalized;

            //Debug.Log(inputDir + " | " + new Vector2(x, z).magnitude);

            anim.SetFloat("run", new Vector2(x, z).magnitude);

            if (inputDir != Vector2.zero)
            {
                float targetRotation = Mathf.Atan2(inputDir.x, inputDir.y) * Mathf.Rad2Deg + Camera.main.transform.eulerAngles.y;
                transform.eulerAngles = Vector3.up * Mathf.SmoothDampAngle(transform.eulerAngles.y, targetRotation, ref turnSmoothVelocity, turnSmoothTime);
            }

            currentSpeed = Mathf.SmoothDamp(currentSpeed, speed * inputDir.magnitude, ref speedSmoothVelocity, speedSmoothTime);
            characterController.Move(transform.forward * currentSpeed * Time.deltaTime);
            if (characterController.velocity.magnitude>0&&!AudioControllerPlayer.Instance.m_AudioSourceWalkSFX.isPlaying)
            {
                AudioControllerPlayer.Instance.Walk(true);
                //AudioControllerPlayer.Instance.m_AudioSourceWalkSFX.clip = AudioControllerPlayer.Instance.m_Walk;
                
                //AudioControllerPlayer.Instance.m_AudioSourceWalkSFX.Play();
            }
            
        }
    }

    private void AttackPlayer()
    {
        if (photonView.IsMine && !isStun && GameController.m_IsGameActive)
        {
            if (Input.GetMouseButtonDown(0) && hasPoweUp)
            {
                photonView.RPC("RPC_Attack", RpcTarget.AllBuffered);
                //hasPoweUp = false; //SE VUELVE FALSO EN EL SCRIPT PUNCH
            }
        }
    }

    private IEnumerator ActiveAttack()
    {
        yield return new WaitForSeconds(0.5f);
        objectAttack.SetActive(false);
    }


    private void OnTriggerEnter(Collider other)
    {
        if (photonView.IsMine)
        {
            if (other.CompareTag("Key"))
            {
                
                if (numberOfKeys < KeysRespawnController.Instance.m_KeysCount)
                {
                    var obj = other.gameObject.GetComponent<KeyController>();
                    if (obj.getKeyState() == KeyController.keyState.free)
                    {



                        obj.ChangeState(KeyController.keyState.occupied, photonView.ViewID);
                        nameKeysInBag.Add(obj.name);
                        obj.gameObject.SetActive(false);

                        photonView.RPC("RPC_GetKey", RpcTarget.AllBuffered, obj.name, obj.whoHasIt);
                        photonView.RPC("RPC_RefreshClientKeysState", RpcTarget.AllBuffered);
                    }
                }
                if (numberOfKeys == 4)
                {
                    completeKey = true;
                }
            }

            if (other.CompareTag("PowerUp"))
            {
                if (completeKey == false)
                {
                    hasPoweUp = true;
                    GameController.Instance.EnableAndDisableHit(1);
                    AudioControllerPlayer.Instance.PowerUpSFX();
                    
                    photonView.RPC("RPC_DisablePoweUp", RpcTarget.AllBuffered, other.name, false);
                }
            }

            if (other.CompareTag("Attack"))
            {
                if (photonView.IsMine)
                {
                    photonView.RPC("RPC_GetDamage", RpcTarget.AllBuffered);
                }
            }

            if (other.CompareTag("Finish"))
            {
                if (completeKey == true)
                {
                    photonView.RPC("RPC_FinishGame", RpcTarget.AllBuffered, PhotonNetwork.LocalPlayer.NickName);
                }                    
            }
        }
        
    }

    public void SetInitPlayerPosition()
    {
        transform.position = positionInitPlayer;
    }

    public void SetData(string name)
    {
        photonView.RPC("RPC_SetDataToNameTag", RpcTarget.AllBuffered, name);
    }

    public void PoweUpUsed()
    {
        hasPoweUp = false;
        GameController.Instance.EnableAndDisableHit(0);
        //yield return new WaitForSeconds(5f);
        photonView.RPC("RPC_DisablePoweUp", RpcTarget.AllBuffered, currentNamePoweUp, true);
    }


    [PunRPC]
    public void RPC_GetDamage()
    {
        if (numberOfKeys > 0)
        {
            for (int i = 0; i < keysInBag.Count; i++)
            {
                keysInBag[i].SetActive(false);
                speed = 5;
            }
            numberOfKeys = 0;
            completeKey = false;
            if (photonView.IsMine)
            {
                //Descarte de aleatoriedad

                //for (int i = 0; i < KeysRespawnController.Instance.keys.Count; i++)
                //{
                //    var pos = KeysRespawnController.Instance.GetKeySpawnAvailable().gameObject.name;

                //    photonView.RPC("RPC_ReappearanceOfKeysTogether", RpcTarget.AllBuffered, photonView.ViewID, i, pos);

                //}

                //Las llaves vuelven a sus posiciones estáticas iniciales 
                AudioControllerPlayer.Instance.KeySFX();
                foreach (var key in KeysRespawnController.Instance.keys)
                {
                    foreach(var nameKey in nameKeysInBag)
                    {
                        if (key.name == nameKey)
                        {
                            //key.GetComponent<KeyController>().ChangeState(KeyController.keyState.free,0);
                            photonView.RPC("RPC_RestoreKey", RpcTarget.AllBuffered, key.name);
                        }
                    }
                    
                }
                nameKeysInBag.Clear();

            }
        }

        if(!isStun)
        {
            AudioControllerPlayer.Instance.HitPunch();
            AudioControllerPlayer.Instance.StunSFX();
            anim.SetBool("stun", true);
            isStun = true;
            StartCoroutine(ReactiveMovement());
        }
        
    }

    IEnumerator ReactiveMovement()
    {
        yield return new WaitForSeconds(2.0f);
        anim.SetBool("stun", false);
        isStun = false;
    }
    
    [PunRPC]
    public void RPC_Attack()
    {
        if (photonView.IsMine)
        {
            Debug.Log("estoy atacando");
            AudioControllerPlayer.Instance.SwingPunch();
        }
            
        else
            Debug.Log("el jugador " + photonView.ViewID + "está atancando");

        anim.SetTrigger("punch");
        StartCoroutine(ActiveAttack());
        objectAttack.SetActive(true);
    }

    [PunRPC]
    public void RPC_GetKey(string RefPositionName, int idPlayer)
    {
        KeysRespawnController.Instance.UnabledKey(RefPositionName, idPlayer);
        AudioControllerPlayer.Instance.KeySFX();

    }

    [PunRPC]
    public void RPC_RefreshClientKeysState()
    {
        keysInBag[numberOfKeys].SetActive(true);
        numberOfKeys++;
        speed--;
    }

    

    [PunRPC]
    public void RPC_RestoreKey(string name)
    {
        //KeysRespawnController.Instance.RestoreKey(photonId, name);
        KeysRespawnController.Instance.EnabledKey(name);
    }


    //NO USADO
    [PunRPC]
    public void RPC_KeysRespawn(int indexKey, string newPos)
    {
        KeysRespawnController.Instance.RandomSetKeysPositions(indexKey,newPos);
    }

    //NO USADO
    [PunRPC]
    public void RPC_ReappearanceOfKeysTogether(int photonId,int indexKey, string newPos)
    {
        KeysRespawnController.Instance.GG(photonId, indexKey, newPos);
    }


    [PunRPC]
    public void RPC_FinishGame(string winnerName)
    {
        GameController.m_GameWinnerName = winnerName;
        GameController.Instance.FinishGame();
    }

    [PunRPC]
    public void RPC_SetDataToNameTag(string Name)
    {
        if (string.IsNullOrEmpty(Name))
            nameText.text = "name " + Random.Range(100, 300);
        else
            nameText.text = Name;
    }

    [PunRPC]
    public void RPC_SetColorToNameTag(int indexColor)
    {
        nameText.color = m_PlayerColors[indexColor];
        m_PlayerMesh.materials[0].color = m_PlayerColors[indexColor];
    }

    [PunRPC]
    public void RPC_DisablePoweUp(string name, bool value)
    {
        PoweUpController.Instance.EnableAndDisablePowerUp(name,value);
        currentNamePoweUp = name;

    }


}
